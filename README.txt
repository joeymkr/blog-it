Name: Joe Roberts
Student ID: M00422701
Module Code: CMT3315

I hereby confirm that the work presented here in this report and in all 
other associated material is wholly my own work. I confirm that the report
has been submitted to TURNITIN. I agree to an assessment for plagiarism.

Joe Roberts


/******************************RUN**************************************

To run the website place the folder "Blog-it" in the root directory of your 
server. For the best performance please use the Google Chrome web browser 
or Firefox.

ONCE THE SERVER IS RUNNING AND "Blog-it" IS IN THE ROOT FOLDER
Type the following into the browsers address bar :
	
	localhost/blog-it

The website can also been found online at : 

	http://joe-roberts.com.uksite4.yourwebservers.com/blog-it/

/*****************************FILES*************************************

The DTD & XSD are located in "Blog-it"
The xml file is located in "Blog-it/xml"