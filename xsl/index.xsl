<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <div id="header">
                <div class="wrap">
                    <div id="logo"><h1>Blog-It</h1></div>
                    <div id="navigation">
                        <ul>
                            <li><a href="../blog-it">Home</a></li>
                            <li><a href="library">Library</a></li>
                            <li><a>
                                <xsl:attribute name="href">
                                    blog/?id=<xsl:value-of select="blog/post[last()]/@id"/>
                                </xsl:attribute>
                                Latest
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="main-body">
                <div id="main-body-container" class="wrap">
                    <div id="left-box">
                        <div id="topbox">
                            <h3>Welcome</h3>
                            <p>Blog-It is a free photography blog site for amateur photographers and hobbyists.
                            </p>
                            <p>Our photography is mainly studio based fashion and portraiture but we do venture into the unknown</p>
                        </div>
                        <div id="bottombox">
                            <h3>Contact</h3>
                            <p>Email : info@blog-it.co.uk</p>
                            <p>Phone : 0844 659 123</p>
                            <address>Blog-it ltd<br/> 4 Sanctions Rd, <br/>London, <br/>SE1 1AA</address>
                        </div>
                    </div>
                    <div id="right-box">
                        <a>
                        <xsl:attribute name="href">
                            blog/?id=<xsl:value-of select="blog/post[last()]/@id"/>
                        </xsl:attribute>
                            <div class="right-inner-box" style="background-image: url({blog/post[last()]/image})">
                                <div class="descriptor-box">
                                    <h3>
                                        <xsl:value-of select="/blog/post[last()]/title"/>
                                    </h3>
                                    <p>
                                        <xsl:value-of select="/blog/post[last()]/paragraph"/>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <a>
                        <xsl:attribute name="href">
                            blog/?id=<xsl:value-of select="blog/post[last()-1]/@id"/>
                        </xsl:attribute>
                            <div class="right-inner-box" style="background-image: url({blog/post[last()-1]/image})">
                                <div class="descriptor-box">
                                    <h3>
                                        <xsl:value-of select="/blog/post[last()-1]/title"/>
                                    </h3>
                                    <p>
                                        <xsl:value-of select="/blog/post[last()-1]/paragraph"/>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <a>
                        <xsl:attribute name="href">
                            blog/?id=<xsl:value-of select="blog/post[last()-2]/@id"/>
                        </xsl:attribute>
                            <div class="right-inner-box" style="background-image: url({blog/post[last()-2]/image})">
                                <div class="descriptor-box">
                                    <h3>
                                        <xsl:value-of select="/blog/post[last()-2]/title"/>
                                    </h3>
                                    <p>
                                        <xsl:value-of select="/blog/post[last()-2]/paragraph"/>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <a>
                        <xsl:attribute name="href">
                            blog/?id=<xsl:value-of select="blog/post[last()-3]/@id"/>
                        </xsl:attribute>
                            <div class="right-inner-box" style="background-image: url({blog/post[last()-3]/image})">
                                <div class="descriptor-box">
                                    <h3>
                                        <xsl:value-of select="/blog/post[last()-3]/title"/>
                                    </h3>
                                    <p>
                                        <xsl:value-of select="/blog/post[last()-3]/paragraph"/>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div id="footer-main">
                <div class="wrap"></div>
            </div>
            
    </xsl:template>

</xsl:stylesheet>
