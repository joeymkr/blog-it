<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <div id="header">
            <div class="wrap">
                <div id="logo"><h1>Blog-It</h1></div>
                <div id="navigation">
                    <ul>
                        <li><a href="../">Home</a></li>
                        <li><a href="../library/">Library</a></li>
                        <li><a href="../library/">Latest</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="main-body">
            <div id="main-body-container" class="wrap">
                <div id="left-box">
                </div>
                <div id="right-box">
                    <div class="descriptor-box-wide">
                        <h5>
                            Date:
                            <xsl:value-of select="/blog/post[1]/date"/>
                        </h5>
                        <h5>
                            Author:
                            <xsl:value-of select="/blog/post[1]/author"/>
                        </h5>
                        <h1>
                            <xsl:value-of select="/blog/post[1]/title"/>
                        </h1>
                        <p>
                            <xsl:value-of select="/blog/post[1]/paragraph"/>
                        </p>
                     </div>
                    <div class="right-inner-box-centre">
                        <image src="{blog/post[1]/image}"></image>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="wrap"></div>
        </div>

    </xsl:template>

</xsl:stylesheet>
