<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <div id="header">
            <div class="wrap">
                <div id="logo"><h1>Blog-It</h1></div>
                <div id="navigation">
                    <ul>
                        <li><a href="../">Home</a></li>
                        <li><a href="../library/">Library</a></li>
                        <li><a>
                            <xsl:attribute name="href">
                                ../blog/?id=<xsl:value-of select="blog/post[last()]/@id"/>
                            </xsl:attribute>
                            Latest
                        </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="main-body">
            <div id="main-body-container" class="wrap">
                <div id="left-box">
                    <div id="topbox">
                        <h3>Library</h3>
                        <p>Please choose from our library of articles.
                        </p>
                        <p>Our topics are focused on just about anything and everything there is in photography.</p>
                    </div>
                    <div id="bottombox">
                        <h3>Contact</h3>
                        <p>Email : info@blog-it.co.uk</p>
                        <p>Phone : 0844 659 123</p>
                        <address>Blog-it ltd<br/> 4 Sanctions Rd, <br/>London, <br/>SE1 1AA</address>
                    </div>
                </div>
                <div id="right-box">
                    <xsl:for-each select="blog/post">
                        <a>
                           <xsl:attribute name="href">
                               ../blog/?id=<xsl:value-of select="@id"/>
                           </xsl:attribute>
                            <div class="right-inner-box-wide" style="background-image: url('../{image}')">
                                <div class="descriptor-box">
                                    <h3>
                                        <xsl:value-of select="title"/>
                                    </h3>
                                    <p>
                                        <xsl:value-of select="paragraph"/>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <div class="spacer"></div>
                    </xsl:for-each>
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="wrap"></div>
        </div>

    </xsl:template>

</xsl:stylesheet>
