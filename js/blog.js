window.onload = displayPage();
var xml;
var xsl;

function displayPage()
{
//loads xml documents
    xml = loadXMLDoc("../xml/blog.xml");
    xsl = loadXMLDoc("../xsl/blog.xsl");

    id = getId();

    blog = xml.getElementsByTagName("post")[id];

    xmlString =
        '<?xml version="1.0" encoding="UTF-8"?>' +
            '<blog>' +
                '<post>' +
                    '<title>' + blog.childNodes[1].firstChild.nodeValue + '</title>' +
                    '<date>' + blog.childNodes[3].firstChild.nodeValue + '</date>' +
                    '<author>' + blog.childNodes[5].firstChild.nodeValue + '</author>' +
                    '<paragraph>' + blog.childNodes[7].firstChild.nodeValue + '</paragraph>' +
                    '<image>' + '../' + blog.childNodes[9].firstChild.nodeValue + '</image>' +
                '</post>' +
            '</blog>';

    convertStringToXml = stringToXML(xmlString);
    
//IE
    if (window.ActiveXObject || xhttp.responseType == "msxml-document")
    {
        ex = convertStringToXml.transformNode(xsl);
        document.getElementById("container").innerHTML = ex;
    }
//Chrome, Opera, FireFox, Safari
    else if(document.implementation && document.implementation.createDocument)
    {
        xsltProcessor = new XSLTProcessor();
        xsltProcessor.importStylesheet(xsl);
        resultDocument = xsltProcessor.transformToFragment(convertStringToXml, document);
        document.getElementById("container").appendChild(resultDocument);
    }
    
}

function getId() {
    var result = "Not found",
        tmp = [];
//  take query string and split it
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
//      split each property = value
        tmp = items[index].split("=");
//      take the value which is the id
        result = decodeURIComponent(tmp[1]);
    }
    return result;
}