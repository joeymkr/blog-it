window.onload = displayPage();
var xml;
var xsl;

function displayPage()
{
//loads xml documents
    xml = loadXMLDoc("../xml/blog.xml");
    xsl = loadXMLDoc("../xsl/library.xsl");
    
//IE
    if (window.ActiveXObject || xhttp.responseType == "msxml-document")
    {
        ex = xml.transformNode(xsl);
        document.getElementById("container").innerHTML = ex;
    }
//Chrome, Opera, FireFox, Safari
    else if(document.implementation && document.implementation.createDocument)
    {
        xsltProcessor = new XSLTProcessor();
        xsltProcessor.importStylesheet(xsl);
        resultDocument = xsltProcessor.transformToFragment(xml, document);
        document.getElementById("container").appendChild(resultDocument);
    }
    
}